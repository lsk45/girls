from django.utils import timezone
from django.urls import reverse
from django.views.generic import CreateView, DetailView, ListView, UpdateView
from .models import Post


class PostListView(ListView):

    def get_queryset(self):
        return Post.objects.filter(published_date__lte=timezone.now())


class PostDetailView(DetailView):

    def get_queryset(self):
        return Post.objects.filter(published_date__lte=timezone.now())

class PostCreateView(CreateView):
    model = Post
    fields = 'title', 'content'

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.published_date = timezone.now()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('blog:post_detail', kwargs={'pk': self.object.id})

class PostEditView(UpdateView):
    model = Post
    fields = 'title', 'content'

    def get_success_url(self):
        return reverse('blog:post_detail', kwargs={'pk': self.object.id})
